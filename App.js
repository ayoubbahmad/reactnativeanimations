import React, { Component } from 'react';
import AppNavigator from './App/routes/AppNavigator';
import { Opacity, TranslatePosition } from './App/screens/animationsSceens';

export default class App extends Component {
	constructor(props) {
		super(props);
		console.disableYellowBox = true;
	}

	render() {
		return (
			<AppNavigator />
			// <Opacity />

			// <TranslatePosition />
		);
	}
}
