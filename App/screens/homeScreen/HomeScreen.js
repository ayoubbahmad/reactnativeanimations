import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';

export default class HomeScreen extends Component {
	static navigationOptions = {
		title: 'Home'
	};

	render() {
		const animations = [
			{ screenName: 'Opacity', screenTitle: 'Opacity' },
			{ screenName: 'TranslatePosition', screenTitle: 'Translate Position' }
		];
		return (
			<View style={styles.container}>
				<FlatList
					keyExtractor={({ item, index }) => 'key' + index}
					data={animations}
					renderItem={({ item, index }) => {
						return (
							<TouchableOpacity
								style={styles.card}
								onPress={() => {
									this.props.navigation.navigate(item.screenName);
								}}>
								<View
									style={{
										backgroundColor: '#715a8d',
										height: 50,
										borderTopLeftRadius: 15,
										borderBottomLeftRadius: 15,
										alignItems: 'center',
										justifyContent: 'center',
										width: 50
									}}>
									<Text style={{ color: '#ffffff', fontSize: 25 }}>
										{++index}
									</Text>
								</View>
								<Text style={styles.cardText}>
									{item.screenTitle.toUpperCase()}
								</Text>
							</TouchableOpacity>
						);
					}}
				/>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		paddingTop: 26,
		flex: 1,
		backgroundColor: '#e7ecf7'
	},
	card: {
		flexDirection: 'row',
		backgroundColor: '#422e58',
		borderRadius: 15,
		margin: 16,
		marginTop: 0,
		shadowColor: 'black',
		shadowOffset: { height: 3, width: 0 },
		shadowOpacity: 0.2,
		shadowRadius: 1,
		alignItems: 'center'
	},
	cardText: {
		fontSize: 20,
		color: '#ffffff',
		fontWeight: '400',
		marginLeft: 16
	}
});
