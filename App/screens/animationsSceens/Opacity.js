import React, { Component } from 'react';
import {
	Text,
	View,
	StyleSheet,
	ImageBackground,
	Animated
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class Opacity extends Component {
	state = {
		animation: new Animated.Value(1),
		imageHiden: false
	};
	render() {
		const { animation, imageHiden } = this.state;
		return (
			<View style={styles.container}>
				<Animated.View style={{ opacity: animation }}>
					<ImageBackground
						style={{ width: 100, height: 100 }}
						source={require('../../res/images/cartoon.png')}
					/>
				</Animated.View>
				<TouchableOpacity
					onPress={() => {
						if (imageHiden)
							Animated.timing(animation, {
								toValue: 1,
								duration: 1000
							}).start(() => {
								this.setState({ imageHiden: !this.state.imageHiden });
							});
						else
							Animated.timing(animation, {
								toValue: 0,
								duration: 1000
							}).start(() => {
								this.setState({ imageHiden: !this.state.imageHiden });
							});
					}}
					style={{
						height: 30,
						backgroundColor: '#715a8d',
						borderRadius: 10,
						justifyContent: 'center',
						padding: 6,
						marginTop: 10
					}}>
					<Text style={{ color: 'white' }}>
						{imageHiden ? 'Bring Me Back 🥺' : 'Click Here To Hide Me 🤪'}
					</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		justifyContent: 'center',
		alignItems: 'center',
		flex: 1,
		backgroundColor: '#e7ecf7'
	}
});
