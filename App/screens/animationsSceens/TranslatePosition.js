import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Animated, { Easing } from 'react-native-reanimated';

export default class TranslatePosition extends Component {
	state = {
		positionX: new Animated.Value(0),
		positionY: new Animated.Value(0),
		ballHeight: new Animated.Value(60),
		ballWidth: new Animated.Value(60)
	};

	move = (deltaX, deltaY) => {
		newXPosition = this.state.positionX._value + deltaX;
		newYPosition = this.state.positionY._value + deltaY;
		let easing = Easing.bounce;
		let duration = 1000;
		if (
			(newYPosition == 0 && newXPosition == 0) ||
			(this.state.positionX._value != 0 && newXPosition == 0) ||
			(this.state.positionY._value != 0 && newYPosition == 0)
		)
			easing = Easing.elastic(2.5);
		Animated.timing(this.state.positionX, {
			toValue: newXPosition,
			duration: duration,
			easing: easing
		}).start(() => {});
		Animated.timing(this.state.positionY, {
			toValue: newYPosition,
			duration: duration,
			easing: easing
		}).start(() => {
			this.setState({
				positionX: new Animated.Value(newXPosition),
				positionY: new Animated.Value(newYPosition)
			});
		});
	};
	render() {
		const { positionX, positionY, ballHeight, ballWidth } = this.state;
		return (
			<View style={styles.container}>
				<View
					style={{
						flex: 2,
						borderColor: 'red',
						borderWidth: 3,
						justifyContent: 'center',
						alignItems: 'center'
					}}>
					<Animated.View
						style={{
							backgroundColor: '#422e58',
							height: ballHeight,
							width: ballWidth,
							borderRadius: 30,
							justifyContent: 'center',
							alignItems: 'center',
							transform: [{ translateX: positionX, translateY: positionY }]
						}}>
						<Text style={{ color: 'white', textAlign: 'center' }}>🙅🏻‍♂️</Text>
					</Animated.View>
				</View>
				<View
					style={{
						flex: 1,
						justifyContent: 'center',
						alignItems: 'center'
					}}>
					{/* MOVE_TOP */}
					<View
						style={{
							transform: [{ rotate: '45deg' }]
						}}>
						<View
							style={{
								flexDirection: 'row',
								justifyContent: 'center',
								alignItems: 'center'
							}}>
							<TouchableOpacity
								style={{}}
								onPress={() => {
									this.move(0, -199);
								}}>
								<View
									style={[
										styles.triangle,
										{ transform: [{ rotate: '-45deg' }], margin: 20 }
									]}
								/>
							</TouchableOpacity>
							{/* MOVE_Right */}
							<TouchableOpacity
								onPress={() => {
									this.move(139, 0);
								}}>
								<View
									style={[
										styles.triangle,
										{ transform: [{ rotate: '45deg' }], margin: 20 }
									]}
								/>
							</TouchableOpacity>
						</View>
						<View
							style={{
								flexDirection: 'row',
								justifyContent: 'center',
								alignItems: 'center'
							}}>
							<TouchableOpacity
								onPress={() => {
									this.move(-139, 0);
								}}>
								<View
									style={[
										styles.triangle,
										{ transform: [{ rotate: '-135deg' }], margin: 20 }
									]}
								/>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={() => {
									this.move(0, 199);
								}}>
								<View
									style={[
										styles.triangle,
										{ transform: [{ rotate: '135deg' }], margin: 20 }
									]}
								/>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 16
	},
	triangle: {
		borderLeftWidth: 20,
		borderRightWidth: 20,
		borderTopWidth: 0,
		borderWidth: 40,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: 'red'
	}
});
