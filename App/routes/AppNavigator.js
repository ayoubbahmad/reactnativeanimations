import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from '../screens/homeScreen/HomeScreen';
import Opacity from '../screens/animationsSceens/Opacity';
import TranslatePosition from '../screens/animationsSceens/TranslatePosition';

const AppNavigator = createStackNavigator(
	{
		Home: {
			screen: HomeScreen
		},
		Opacity: {
			screen: Opacity
		},
		TranslatePosition: {
			screen: TranslatePosition
		}
	},
	{
		defaultNavigationOptions: {
			headerTintColor: '#fff',
			headerStyle: {
				backgroundColor: '#422e58'
			}
		}
	}
);

export default createAppContainer(AppNavigator);
